
package ru.leonova.tm.api.endpoint;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ru.leonova.tm.api.endpoint package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Exception_QNAME = new QName("http://endpoint.api.tm.leonova.ru/", "Exception");
    private final static QName _CloseSession_QNAME = new QName("http://endpoint.api.tm.leonova.ru/", "closeSession");
    private final static QName _CloseSessionResponse_QNAME = new QName("http://endpoint.api.tm.leonova.ru/", "closeSessionResponse");
    private final static QName _GetSessionByUserId_QNAME = new QName("http://endpoint.api.tm.leonova.ru/", "getSessionByUserId");
    private final static QName _GetSessionByUserIdResponse_QNAME = new QName("http://endpoint.api.tm.leonova.ru/", "getSessionByUserIdResponse");
    private final static QName _OpenSession_QNAME = new QName("http://endpoint.api.tm.leonova.ru/", "openSession");
    private final static QName _OpenSessionResponse_QNAME = new QName("http://endpoint.api.tm.leonova.ru/", "openSessionResponse");
    private final static QName _Valid_QNAME = new QName("http://endpoint.api.tm.leonova.ru/", "valid");
    private final static QName _ValidResponse_QNAME = new QName("http://endpoint.api.tm.leonova.ru/", "validResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.leonova.tm.api.endpoint
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Exception }
     * 
     */
    public Exception createException() {
        return new Exception();
    }

    /**
     * Create an instance of {@link CloseSession }
     * 
     */
    public CloseSession createCloseSession() {
        return new CloseSession();
    }

    /**
     * Create an instance of {@link CloseSessionResponse }
     * 
     */
    public CloseSessionResponse createCloseSessionResponse() {
        return new CloseSessionResponse();
    }

    /**
     * Create an instance of {@link GetSessionByUserId }
     * 
     */
    public GetSessionByUserId createGetSessionByUserId() {
        return new GetSessionByUserId();
    }

    /**
     * Create an instance of {@link GetSessionByUserIdResponse }
     * 
     */
    public GetSessionByUserIdResponse createGetSessionByUserIdResponse() {
        return new GetSessionByUserIdResponse();
    }

    /**
     * Create an instance of {@link OpenSession }
     * 
     */
    public OpenSession createOpenSession() {
        return new OpenSession();
    }

    /**
     * Create an instance of {@link OpenSessionResponse }
     * 
     */
    public OpenSessionResponse createOpenSessionResponse() {
        return new OpenSessionResponse();
    }

    /**
     * Create an instance of {@link Valid }
     * 
     */
    public Valid createValid() {
        return new Valid();
    }

    /**
     * Create an instance of {@link ValidResponse }
     * 
     */
    public ValidResponse createValidResponse() {
        return new ValidResponse();
    }

    /**
     * Create an instance of {@link Session }
     * 
     */
    public Session createSession() {
        return new Session();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Exception }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.leonova.ru/", name = "Exception")
    public JAXBElement<Exception> createException(Exception value) {
        return new JAXBElement<Exception>(_Exception_QNAME, Exception.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CloseSession }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.leonova.ru/", name = "closeSession")
    public JAXBElement<CloseSession> createCloseSession(CloseSession value) {
        return new JAXBElement<CloseSession>(_CloseSession_QNAME, CloseSession.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CloseSessionResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.leonova.ru/", name = "closeSessionResponse")
    public JAXBElement<CloseSessionResponse> createCloseSessionResponse(CloseSessionResponse value) {
        return new JAXBElement<CloseSessionResponse>(_CloseSessionResponse_QNAME, CloseSessionResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetSessionByUserId }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.leonova.ru/", name = "getSessionByUserId")
    public JAXBElement<GetSessionByUserId> createGetSessionByUserId(GetSessionByUserId value) {
        return new JAXBElement<GetSessionByUserId>(_GetSessionByUserId_QNAME, GetSessionByUserId.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetSessionByUserIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.leonova.ru/", name = "getSessionByUserIdResponse")
    public JAXBElement<GetSessionByUserIdResponse> createGetSessionByUserIdResponse(GetSessionByUserIdResponse value) {
        return new JAXBElement<GetSessionByUserIdResponse>(_GetSessionByUserIdResponse_QNAME, GetSessionByUserIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpenSession }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.leonova.ru/", name = "openSession")
    public JAXBElement<OpenSession> createOpenSession(OpenSession value) {
        return new JAXBElement<OpenSession>(_OpenSession_QNAME, OpenSession.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpenSessionResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.leonova.ru/", name = "openSessionResponse")
    public JAXBElement<OpenSessionResponse> createOpenSessionResponse(OpenSessionResponse value) {
        return new JAXBElement<OpenSessionResponse>(_OpenSessionResponse_QNAME, OpenSessionResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Valid }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.leonova.ru/", name = "valid")
    public JAXBElement<Valid> createValid(Valid value) {
        return new JAXBElement<Valid>(_Valid_QNAME, Valid.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ValidResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.leonova.ru/", name = "validResponse")
    public JAXBElement<ValidResponse> createValidResponse(ValidResponse value) {
        return new JAXBElement<ValidResponse>(_ValidResponse_QNAME, ValidResponse.class, null, value);
    }

}
