package ru.leonova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.api.endpoint.Session;
import ru.leonova.tm.api.endpoint.Task;
import ru.leonova.tm.command.AbstractCommand;

import java.text.SimpleDateFormat;
import java.util.List;

public class TaskSortByStatus extends AbstractCommand {
    @Override
    public String getName() {
        return "t-sort-by-status";
    }

    @Override
    public String getDescription() {
        return "Sorted projects by status";
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public void execute() throws Exception {
        final Session session = serviceLocator.getCurrentSession();
        if(session==null) throw new Exception("You are have not session");
        @NotNull final List<Task> taskList = serviceLocator.getTaskEndpoint().sortTasksByStatus(session);
        System.out.println("\nAfter Sorting by status:");
        @NotNull final SimpleDateFormat format = new SimpleDateFormat();
        for (@NotNull final Task task : taskList) {
            System.out.println(task.getName() + " " + format.format(task.getDateSystem()));
        }
    }
}
