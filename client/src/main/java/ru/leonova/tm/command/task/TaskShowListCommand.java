package ru.leonova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.api.endpoint.Session;
import ru.leonova.tm.api.endpoint.Task;
import ru.leonova.tm.command.AbstractCommand;

import java.text.SimpleDateFormat;
import java.util.Collection;

public final class TaskShowListCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "list-t";
    }

    @Override
    public String getDescription() {
        return "Show tasks list";
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public void execute() throws Exception {
        final Session session = serviceLocator.getCurrentSession();
        if(session==null) throw new Exception("You are have not session");
        System.out.println("[TASK LIST]");
        @NotNull final Collection<Task> taskCollection = serviceLocator.getTaskEndpoint().getCollection(session);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        int i = 0;
        for (@NotNull final Task task : taskCollection) {
            i++;
            System.out.println(i + ". PROJECT ID: " + task.getProjectId() + ",TASK ID: " + task.getTaskId() + ", TASK NAME: " + task.getName()+ ", Date start: " + format.format(task.getDateStart()) + ", Date end: " +  format.format(task.getDateEnd()));
        }
    }
}
