package ru.leonova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.api.endpoint.Project;
import ru.leonova.tm.api.endpoint.Session;
import ru.leonova.tm.command.AbstractCommand;
import ru.leonova.tm.exeption.EmptyArgumentException;

public class ProjectSearchByName extends AbstractCommand {
    @Override
    public String getName() {
        return "search-p-by-name";
    }

    @Override
    public String getDescription() {
        return "Search project by name";
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public void execute() throws Exception {
        System.out.println(getDescription());
        final Session session = serviceLocator.getCurrentSession();
        if(session==null) throw new Exception("You are have not session");
        System.out.println("\nEnter name project:");
        @NotNull final String projectName = getScanner().nextLine();
        @NotNull final Project project = serviceLocator.getProjectEndpoint().searchProjectByName(session, projectName);
        if(project==null)throw new EmptyArgumentException();
        System.out.println(project.getName()+project.getDateStart()+project.getDateEnd());
    }
}
