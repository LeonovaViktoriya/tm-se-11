package ru.leonova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.api.endpoint.Session;
import ru.leonova.tm.command.AbstractCommand;

public final class TaskDeleteListCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "del-all-t";
    }

    @Override
    public String getDescription() {
        return "Delete list tasks of this user ";
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("["+getDescription().toUpperCase()+"]");
        final Session session = serviceLocator.getCurrentSession();
        if(session==null) throw new Exception("You are have not session");
        serviceLocator.getTaskEndpoint().deleteAllTaskByUserId(session);
        System.out.println("All tasks remove");
    }
}
