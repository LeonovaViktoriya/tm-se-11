package ru.leonova.tm.command.system;

import ru.leonova.tm.command.AbstractCommand;

public final class ExitCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "exit";
    }

    @Override
    public String getDescription() {
        return "Exit the application";
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public void execute() {
        System.exit(0);
    }

}

