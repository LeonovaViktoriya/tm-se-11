package ru.leonova.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.api.endpoint.Session;
import ru.leonova.tm.api.endpoint.User;
import ru.leonova.tm.exeption.EmptyArgumentException;
import ru.leonova.tm.utils.PasswordHashUtil;
import ru.leonova.tm.command.AbstractCommand;

import java.util.Objects;

public final class UserUpdatePasswordCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "up-pass";
    }

    @Override
    public String getDescription() {
        return "Update user password";
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("["+getDescription().toUpperCase()+"]");
        final Session session = serviceLocator.getCurrentSession();
        if(session==null) throw new Exception("You are have not session");
        @NotNull final User currentUser = serviceLocator.getUserEndpoint().getCurrentUser(session);
        if(currentUser.getPassword().isEmpty())return;
        System.out.println("Enter new password");
        @NotNull String password = getScanner().nextLine();
        if (password==null) throw new EmptyArgumentException();
        password = Objects.requireNonNull(PasswordHashUtil.md5(password));
        currentUser.setPassword(password);
        System.out.println("Password is updated");
    }
}
