package ru.leonova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.api.endpoint.Project;
import ru.leonova.tm.api.endpoint.Session;
import ru.leonova.tm.command.AbstractCommand;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

public final class ProjectCreateCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "create-p";
    }

    @Override
    public String getDescription() {
        return "Create project";
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public void execute() throws Exception{
        final Session session = serviceLocator.getCurrentSession();
        if(session==null) throw new Exception("You are have not session");
        serviceLocator.getSessionEndpoint().valid(session);
        System.out.print("[CREATE PROJECT]\nINTER NAME: \n");
        @NotNull final String projectName = getScanner().nextLine();
        @NotNull final Project project = new Project();
        project.setName(projectName);
        project.setStatus("planned");
        System.out.println("date start project:");
        @NotNull String dateStartProject = getScanner().nextLine();
        System.out.println("date end project:");
        @NotNull String dateEndProject = getScanner().nextLine();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = simpleDateFormat.parse(dateStartProject);
        GregorianCalendar gregorianCalendar = (GregorianCalendar)GregorianCalendar.getInstance();
        gregorianCalendar.setTime(date);
        XMLGregorianCalendar xmlGregorianCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(gregorianCalendar);
        Date date2 = simpleDateFormat.parse(dateEndProject);
        GregorianCalendar gregorianCalendar2 = (GregorianCalendar)GregorianCalendar.getInstance();
        gregorianCalendar2.setTime(date2);
        Date date3 = new Date();
        GregorianCalendar gregorianCalendar3 = (GregorianCalendar)GregorianCalendar.getInstance();
        gregorianCalendar3.setTime(date3);
        XMLGregorianCalendar xmlGregorianCalendar2 = DatatypeFactory.newInstance().newXMLGregorianCalendar(gregorianCalendar2);
        XMLGregorianCalendar xmlGregCalDateSystem =  DatatypeFactory.newInstance().newXMLGregorianCalendar(gregorianCalendar3);
        serviceLocator.getProjectEndpoint().createProject(session, project, xmlGregorianCalendar, xmlGregorianCalendar2, xmlGregCalDateSystem);
        System.out.println("Project created");
    }
}
