package ru.leonova.tm.command.data.xml;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.dataformat.xml.ser.ToXmlGenerator;
import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.api.endpoint.Domain;
import ru.leonova.tm.api.endpoint.Session;
import ru.leonova.tm.command.AbstractCommand;

import java.io.File;

public class DomainJaksonSaveToXMLCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "jackson-save-xml";
    }

    @Override
    public String getDescription() {
        return "Save data to xml file via jackson";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public void execute() throws Exception {
        final Session session = serviceLocator.getCurrentSession();
        @NotNull final Domain domain = new Domain();
        serviceLocator.getDomainEndpoint().save(session, domain);
        @NotNull File file = new File("./data/jackson.xml");
        @NotNull final ObjectMapper objectMapper = new XmlMapper().configure(ToXmlGenerator.Feature.WRITE_XML_1_1, true);
        @NotNull final ObjectWriter objectWriter = objectMapper.writerWithDefaultPrettyPrinter();
        objectWriter.writeValue(file, domain);
        System.out.println("Data saved to " + file.getPath());
    }
}
