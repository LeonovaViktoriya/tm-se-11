package ru.leonova.tm.utils;

import com.mysql.jdbc.Connection;
import org.jetbrains.annotations.NotNull;

import java.sql.DriverManager;
import java.sql.SQLException;

public class DbConnectionUtil {

    private DbConnectionUtil() {
    }

    @NotNull
    public static Connection getConnection() {
        try {
            Class.forName("org.mysql.Driver");
            System.out.println("Драйвер подключен");
            //Создаём соединение
            Connection connection = (Connection) DriverManager.getConnection("localhost:61381", "root", "root");
            System.out.println("Соединение установлено");
            return connection;
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

}
