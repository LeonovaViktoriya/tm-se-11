package ru.leonova.tm.endpoint;


import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.api.endpoint.ISessionEndpoint;
import ru.leonova.tm.api.service.ISessionService;
import ru.leonova.tm.entity.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(endpointInterface = "ru.leonova.tm.api.endpoint.ISessionEndpoint")
public class SessionEndpoint implements ISessionEndpoint {
    private ISessionService sessionService;

    public static final String SESSION_URL = "http://localhost:8080/SessionEndpoint?wsdl";

    public SessionEndpoint(ISessionService sessionService) {
        this.sessionService = sessionService;
    }

    @Override
    @WebMethod
    public boolean valid(@WebParam(name = "session") @NotNull Session session) throws Exception {
        return sessionService.valid(session);
    }

    @Override
    @WebMethod
    public void closeSession(@WebParam(name = "session") @NotNull Session session) throws Exception {
        sessionService.closeSession(session);
    }

    @Override
    @WebMethod
    public Session openSession(@WebParam(name = "login") @NotNull String login, @NotNull String password) throws Exception {
        return sessionService.openSession(login, password);
    }

    @Override
    @WebMethod
    public Session getSessionByUserId(@WebParam(name = "userId") @NotNull String userId) {
        return sessionService.getSessionByUserId(userId);
    }
}
