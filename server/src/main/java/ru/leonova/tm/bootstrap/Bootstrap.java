package ru.leonova.tm.bootstrap;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.api.ServiceLocator;
import ru.leonova.tm.api.endpoint.IDomainEndpoint;
import ru.leonova.tm.api.endpoint.ISessionEndpoint;
import ru.leonova.tm.api.repository.IProjectRepository;
import ru.leonova.tm.api.repository.ITaskRepository;
import ru.leonova.tm.api.repository.IUserRepository;
import ru.leonova.tm.api.service.*;
import ru.leonova.tm.endpoint.*;
import ru.leonova.tm.repository.ProjectRepository;
import ru.leonova.tm.repository.SessionRepository;
import ru.leonova.tm.repository.TaskRepository;
import ru.leonova.tm.repository.UserRepository;
import ru.leonova.tm.service.*;

import javax.xml.ws.Endpoint;
public final class Bootstrap implements ServiceLocator {

    private final IProjectRepository projectRepository = new ProjectRepository();
    private final ITaskRepository taskRepository = new TaskRepository();
    private final IUserRepository userRepository = new UserRepository();
    private final IProjectService projectService = new ProjectService(projectRepository);
    private final ITaskService taskService = new TaskService(taskRepository, projectRepository);
    private final IUserService userService = new UserService(userRepository);
    private SessionRepository sessionRepository = new SessionRepository();
    private final ISessionService isessionService = new SessionService(this, sessionRepository);
    private final UserEndpoint userEndpoint = new UserEndpoint(userService, isessionService);
    private final ProjectEndpoint projectEndpoint = new ProjectEndpoint(projectService, isessionService);
    private final TaskEndpoint taskEndpoint = new TaskEndpoint(taskService, isessionService);
    private final IDomainService domainService = new DomainService(this);
    private final IDomainEndpoint domainEndpoint = new DomainEndpoint(domainService, isessionService);
    private final ISessionEndpoint sessionEndpoint = new SessionEndpoint(isessionService);

    @NotNull
    @Override
    public final IProjectService getProjectService() {
        return projectService;
    }

    @NotNull
    @Override
    public final IUserService getUserService() {
        return userService;
    }

    @NotNull
    @Override
    public final ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public @NotNull IDomainService getDomainService() {
        return domainService;
    }

    @Override
    public @NotNull ISessionService getSessionService() {
        return isessionService;
    }

    public void init() {
        Endpoint.publish(UserEndpoint.URL_USER, userEndpoint);
        Endpoint.publish(ProjectEndpoint.URL, projectEndpoint);
        Endpoint.publish(TaskEndpoint.URL_TASK, taskEndpoint);
        Endpoint.publish(SessionEndpoint.SESSION_URL, sessionEndpoint);
        Endpoint.publish(DomainEndpoint.DOMAIN_URL, domainEndpoint);
//        Endpoint.publish("http://localhost:3306/UserEndpoint?wsdl", userEndpoint);
    }

}

