package ru.leonova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.entity.User;
import ru.leonova.tm.exeption.EmptyArgumentException;

import java.util.Collection;
import java.util.List;

public interface IUserService {
    User getCurrentUser();


    void create(@NotNull User user);

    void load(@NotNull List<User> list) throws Exception;

    User authorizationUser(@NotNull String login, @NotNull String password) throws Exception;

    boolean isAuth();

    User getById(@NotNull String userId) throws Exception;

    void logOut(@NotNull User user);

    Collection<User> getCollection();

    void adminRegistration(@NotNull String admin, @NotNull String admin1) throws Exception;

    String md5Apache(@NotNull String password) throws Exception;

    void addAll(List<User> users);
}
