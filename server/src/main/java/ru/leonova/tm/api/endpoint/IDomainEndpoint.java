package ru.leonova.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.entity.Domain;
import ru.leonova.tm.entity.Session;
import ru.leonova.tm.exeption.AccessException;
import ru.leonova.tm.exeption.EmptyArgumentException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface IDomainEndpoint {

    @WebMethod
    void save(@WebParam(name = "session") @NotNull Session session, @NotNull Domain domain) throws java.lang.Exception;

    @WebMethod
    void load(@WebParam(name = "session") @NotNull Session session, @NotNull Domain domain) throws java.lang.Exception;
}
