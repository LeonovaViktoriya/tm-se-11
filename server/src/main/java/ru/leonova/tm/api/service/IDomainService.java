package ru.leonova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.entity.Domain;
import ru.leonova.tm.exeption.EmptyArgumentException;

public interface IDomainService {
    void save(@NotNull Domain domain);

    void load(@NotNull Domain domain) throws Exception;

}
