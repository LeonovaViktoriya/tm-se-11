package ru.leonova.tm.api;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.api.service.*;

public interface ServiceLocator {

    @NotNull IProjectService getProjectService();

    @NotNull IUserService getUserService();

    @NotNull ITaskService getTaskService();

    @NotNull IDomainService getDomainService();

    @NotNull ISessionService getSessionService();
}
