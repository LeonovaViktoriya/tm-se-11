package ru.leonova.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.api.repository.IProjectRepository;
import ru.leonova.tm.entity.Project;
import ru.leonova.tm.exeption.EmptyCollectionException;
import ru.leonova.tm.utils.DbConnectionUtil;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.*;

public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    @Override
    public void persist(@NotNull final Project project) throws SQLException {
//        @NotNull final String query = "INSERT INTO app_project (id, name, description, status, dateStart, dateFinish, dateOfCreate, user_id) " +
//                "VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
//
//        @NotNull final PreparedStatement statement = DbConnectionUtil.getConnection().prepareStatement(query);
//        statement.setString(1, project.getProjectId());
//        statement.setString(2, project.getName());
//        statement.setString(3, project.getDescription());
//        statement.setString(4, project.getStatus());
//        statement.setTime(5, project.getDateEnd());
        storage.put(project.getProjectId(), project);
    }

    @Override
    public Collection<Project> findAll() {
        return storage.values();
    }

    @Override
    public Project findOneById(@NotNull final String userId, @NotNull final String projectId) throws Exception {
        if (userId.isEmpty() || projectId.isEmpty()) throw new Exception();
        if (!storage.get(projectId).getUserId().equals(userId)) throw new Exception();
        return storage.get(projectId);

    }

    @Override
    public Collection<Project> findAllByUserId(@NotNull final String userId) throws Exception {
        if (userId.isEmpty()) throw new Exception();
        @NotNull final Collection<Project> projectCollection = new ArrayList<>();
        for (@NotNull final Project project : storage.values()) {
            if (project.getUserId().equals(userId)) {
                projectCollection.add(project);
            }
        }
        return projectCollection;
    }

    @Override
    public void updateProjectName(@NotNull final String userId, @NotNull final String projectId, @NotNull final String name) throws Exception, Exception {
        if (projectId.isEmpty() || name.isEmpty() || userId.isEmpty()) throw new Exception();
        @NotNull final Project project = storage.get(projectId);
        if(!project.getUserId().equals(userId)) throw new Exception();
        project.setName(name);
    }

    public boolean isExist(@NotNull final String projectId) throws Exception {
        if (projectId.isEmpty()) throw new Exception();
        return storage.containsKey(projectId);
    }

    @Override
    public void merge(@NotNull final Project project) throws Exception, Exception, Exception {
        if (project.getProjectId() == null || project.getProjectId().isEmpty()) throw new Exception();
        for (@NotNull final Project project1 : storage.values()) {
            if (project1.getProjectId().equals(project.getProjectId())) {
                updateProjectName(project.getUserId(), project1.getProjectId(), project.getName());
            } else {
                persist(project);
            }
        }
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final Project project) throws Exception {
        if (userId.isEmpty() || project.getProjectId() == null || project.getProjectId().isEmpty()) throw new Exception();
        if (!project.getUserId().equals(userId)) throw new Exception();
        storage.remove(project.getProjectId());
    }

    @Override
    public void removeAllProjectsByUserId(@NotNull final String userId) throws Exception, EmptyCollectionException {
        if (userId.isEmpty()) throw new Exception();
        @NotNull final Collection<Project> projects = findAllByUserId(userId);
        if (projects==null || projects.isEmpty()) throw new EmptyCollectionException();
        projects.clear();
    }

    @Override
    public List<Project> sortedProjectsBySystemDate(@NotNull final String userId) throws Exception, EmptyCollectionException {
        if (userId.isEmpty()) throw new Exception();
        @NotNull final Collection<Project> projectCollection = findAll();
        @NotNull final List<Project> projects = new ArrayList<>(projectCollection);
        if (projects.isEmpty()) throw new EmptyCollectionException();
        @NotNull final Comparator<Project> comparator = Comparator.comparing(Project::getDateSystem);
        projects.sort(comparator);
        return projects;
    }

    @Override
    public List<Project> sortedProjectsByStartDate(@NotNull final String userId) throws Exception, EmptyCollectionException {
        if (userId.isEmpty()) throw new Exception();
        @NotNull final Collection<Project> projectCollection = findAll();
        @NotNull final List<Project> projects = new ArrayList<>(projectCollection);
        if (projectCollection.isEmpty() || projects.isEmpty()) throw new EmptyCollectionException();
        @NotNull final Comparator<Project> comparator = Comparator.comparing(Project::getDateStart);
        projects.sort(comparator);
        return projects;
    }

    @Override
    public List<Project> sortedProjectsByEndDate(@NotNull final String userId) throws Exception, EmptyCollectionException {
        if (userId.isEmpty()) throw new Exception();
        @NotNull final Collection<Project> projectCollection = Objects.requireNonNull(findAllByUserId(userId));
        @NotNull final List<Project> projects = new ArrayList<>(projectCollection);
        if (projects.isEmpty()) throw new EmptyCollectionException();
        @NotNull final Comparator<Project> comparator = Comparator.comparing(Project::getDateEnd);
        projects.sort(comparator);
        return projects;
    }

    @Override
    public List<Project> sortedProjectsByStatus(@NotNull final String userId) throws EmptyCollectionException, Exception {
        if (userId.isEmpty()) throw new Exception();
        @NotNull final Collection<Project> projectCollection = Objects.requireNonNull(findAllByUserId(userId));
        @NotNull final List<Project> projects = new ArrayList<>(projectCollection);
        if (projectCollection.isEmpty() || projects.isEmpty()) throw new EmptyCollectionException();
        @NotNull final Comparator<Project> comparator = Comparator.comparing(Project::getStatus);
        projects.sort(comparator);
        return projects;
    }

    @Override
    public Project searchProjectByName(@NotNull final String userId, @NotNull final String projectName) throws Exception {
        if (projectName.isEmpty() || userId.isEmpty()) throw new Exception();
        for (@NotNull final Project project : storage.values()) {
            if (project.getName().contains(projectName) && project.getUserId().equals(userId)) {
                return project;
            }
        }
        return null;
    }

    @Override
    public Project searchProjectByDescription(@NotNull final String userId, @NotNull final String description) throws Exception {
        if (description.isEmpty() || userId.isEmpty()) throw new Exception();
        for (@NotNull final Project project : storage.values()) {
            if (project.getDescription().contains(description) && project.getUserId().equals(userId)) {
                return project;
            }
        }
        return null;
    }

    @Override
    public void load(@NotNull final List<Project> list) {
        list.forEach(project -> storage.put(project.getProjectId(), project));
    }

}
