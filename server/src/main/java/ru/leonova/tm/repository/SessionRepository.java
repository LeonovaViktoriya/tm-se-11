package ru.leonova.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.api.repository.ISessionRepository;
import ru.leonova.tm.entity.Session;
import ru.leonova.tm.enumerated.RoleType;
import ru.leonova.tm.exeption.EmptyArgumentException;

import java.util.Collection;

public class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {

    @Override
    public void persist(@NotNull final Session session){
        storage.put(session.getSessionId(), session);
    }

    @Override
    public void merge(@NotNull final Session session) throws Exception {
        if (session.getSessionId().isEmpty()) throw new Exception();
        for (@NotNull final Session s : storage.values()) {
            if (s.getSessionId().equals(session.getSessionId())) {
                update(s.getSessionId(), session.getRoleType(), session.getUserId(), session.getSignature(), session.getTimestamp());
            } else {
                persist(session);
            }
        }
    }

    @Override
    public void update(String sessionId, RoleType roleType, String userId, String signature, Long timestamp) {
        Session s = findSessionById(sessionId);
        if(s==null) return;
        s.setRoleType(roleType);
        s.setUserId(userId);
        s.setSignature(signature);
        s.setTimestamp(timestamp);
    }

    @Override
    public Session findSessionById(String sessionId) {
        return storage.get(sessionId);
    }


    @Override
    public Session findSessionByUserId(String userId) {
        for (Session session: storage.values()) {
            if(session.getUserId().equals(userId)) return session;
        }
        return null;
    }

    @Override
    public Session findOne(@NotNull final String sessionId) throws Exception {
        if (sessionId.isEmpty()) throw new Exception();
        return storage.get(sessionId);
    }

    @Override
    public Collection<Session> findAll() {
        return storage.values();
    }

    @Override
    public void remove(@NotNull final Session session) throws Exception {
        if(session.getSessionId().isEmpty()) throw new Exception();
        storage.remove(session.getSessionId());
    }

    @Override
    public void removeAll() {
        storage.clear();
    }
}
