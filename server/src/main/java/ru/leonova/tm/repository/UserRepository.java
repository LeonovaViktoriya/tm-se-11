package ru.leonova.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.api.repository.IUserRepository;
import ru.leonova.tm.entity.User;

import java.util.Collection;
import java.util.List;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {


    @Override
    public void persist(@NotNull final User user) {
        storage.put(user.getUserId(), user);
    }

    @Override
    void merge(@NotNull final User user) throws Exception {
        @NotNull final String userId = user.getUserId();
        if (userId.isEmpty()) throw new Exception();
        if (isExist(userId)) storage.get(userId).setLogin(user.getLogin());
        else persist(user);
    }

    private boolean isExist(@NotNull final String userId) throws Exception {
        if (userId.isEmpty()) throw new Exception();
        return storage.containsKey(userId);
    }

    @Override
    public User findOne(@NotNull final String userId) throws Exception {
        if (userId.isEmpty()) throw new Exception();
        return storage.get(userId);
    }

    @Override
    public Collection<User> findAll() {
        return storage.values();
    }

    @Override
    public void remove(@NotNull final User user) throws Exception {
       if(user.getUserId().isEmpty()) throw new Exception();
       storage.remove(user.getUserId());
    }

    @Override
    public void removeAll() {
        storage.clear();
    }

    @Override
    public void load(@NotNull final List<User> list) {
        list.forEach(user -> storage.put(user.getUserId(), user));
    }

}
