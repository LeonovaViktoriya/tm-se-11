package ru.leonova.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.api.repository.IProjectRepository;
import ru.leonova.tm.api.service.IProjectService;
import ru.leonova.tm.entity.Project;
import ru.leonova.tm.exeption.EmptyCollectionException;

import java.util.Collection;
import java.util.List;

public final class ProjectService extends AbstractService<Project> implements IProjectService {

    final private IProjectRepository projectRepository;
    public ProjectService(IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }


    @NotNull
    @Override
    public Collection<Project> getCollection() {
        return projectRepository.findAll();
    }

    @Override
    public void load(@NotNull final List<Project> list) throws Exception {
        if (list.isEmpty()) throw new Exception();
            projectRepository.load(list);
    }

    @Override
    public void create(@NotNull final String userId, @NotNull final Project project) throws Exception {
        @NotNull final String projectId = project.getProjectId();
        if(userId.isEmpty() || projectId.isEmpty()) throw new Exception();
        if(!project.getUserId().equals(userId)) throw new Exception();
        if (isExist(projectId)) projectRepository.merge(project);
        else projectRepository.persist(project);
    }

    private boolean isExist(@NotNull final String projectId) throws Exception {
        if(projectId.isEmpty()) throw new Exception();
        return projectRepository.isExist(projectId);
    }

    @Override
    public void updateNameProject(@NotNull final String userId, @NotNull final String projectId, @NotNull final String name) throws Exception {
        if(userId.isEmpty() || projectId.isEmpty() || name.isEmpty()) throw new Exception();
        if (projectRepository.findOneById(userId, projectId) != null && projectRepository.findOneById(userId, projectId).getUserId().equals(userId)) {
            projectRepository.updateProjectName(userId, projectId, name);
        }
    }

    @Override
    public Collection<Project> findAllByUserId(@NotNull final String userId) throws Exception {
        if(userId.isEmpty()) throw new Exception();
        return projectRepository.findAllByUserId(userId);
    }

    @Override
    public void deleteProject(@NotNull final String userId, @NotNull final String projectId) throws Exception {
        if(projectId.isEmpty() || userId.isEmpty()) throw new Exception();
        @NotNull final Project project = projectRepository.findOneById(userId, projectId);
        if (project != null && project.getUserId().equals(userId)) projectRepository.remove(userId, project);
    }

    @Override
    public void deleteAllProject(@NotNull final String userId) throws Exception {
        if(userId.isEmpty()) throw new Exception();
        projectRepository.removeAllProjectsByUserId(userId);
    }

    @Override
    public  List<Project> sortProjectsBySystemDate(@NotNull final String userId) throws Exception {
        if(userId.isEmpty()) throw new Exception();
        return projectRepository.sortedProjectsBySystemDate(userId);
    }

    @Override
    public List<Project> sortProjectsByStartDate(@NotNull final String userId) throws Exception {
        if(userId.isEmpty()) throw new Exception();
        return projectRepository.sortedProjectsByStartDate(userId);
    }

    @Override
    public List<Project> sortProjectsByEndDate(@NotNull final String userId) throws Exception {
        if(userId.isEmpty()) throw new Exception();
        return projectRepository.sortedProjectsByEndDate(userId);
    }

    @Override
    public List<Project> sortProjectsByStatus(@NotNull final String userId) throws Exception {
        if(userId.isEmpty()) throw new Exception();
        return projectRepository.sortedProjectsByStatus(userId);
    }

    @Override
    public Project searchProjectByName(@NotNull final String userId, @NotNull final String projectName) throws Exception {
        if(userId.isEmpty() || projectName.isEmpty()) throw new Exception();
        return projectRepository.searchProjectByName(userId, projectName);
    }

    @Override
    public Project searchProjectByDescription(@NotNull final String userId, @NotNull final String description) throws Exception {
        if(userId.isEmpty() || description.isEmpty()) throw new Exception();
        return projectRepository.searchProjectByDescription(description, userId);
    }

    @Override
    public void addAll(List<Project> projects) {
        projects.addAll(projectRepository.findAll());
    }
}
