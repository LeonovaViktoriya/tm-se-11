package ru.leonova.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.api.repository.IProjectRepository;
import ru.leonova.tm.api.repository.ITaskRepository;
import ru.leonova.tm.api.service.ITaskService;
import ru.leonova.tm.entity.Project;
import ru.leonova.tm.entity.Task;
import ru.leonova.tm.exeption.AccessException;
import ru.leonova.tm.exeption.EmptyCollectionException;

import java.util.Collection;
import java.util.List;

public final class TaskService extends AbstractService<Task> implements ITaskService {

    final private ITaskRepository taskRepository;
    final private IProjectRepository projectRepository;

    public TaskService(ITaskRepository taskRepository, ru.leonova.tm.api.repository.IProjectRepository iProjectRepository) {
        this.taskRepository = taskRepository;
        projectRepository = iProjectRepository;
    }

    @Override
    public void create(@NotNull final String userId, @NotNull final Task task) throws Exception {
        if(userId.isEmpty()) throw new Exception();
        if (task.getUserId().equals(userId)) taskRepository.merge(task);
    }

    @NotNull
    @Override
    public Collection<Task> getCollection() {
        return taskRepository.findAll();
    }

    @Override
    public void load(@NotNull List<Task> list) throws Exception {
        if (list.isEmpty()) throw new Exception();
        taskRepository.load(list);
    }

    @Override
    public void updateTaskName(@NotNull String userId, @NotNull final String taskId, @NotNull final String taskName) throws Exception {
        if (taskName.isEmpty() || taskId.isEmpty() || taskId.equals(userId)) throw new Exception();
        @NotNull final Task task = taskRepository.findOne(taskId);
        if(task!=null) task.setName(taskName);
    }

    @Override
    public void deleteTasksByIdProject(@NotNull final String userId, @NotNull final String projectId) throws Exception {
        if(projectId.isEmpty() || userId.isEmpty()) throw new Exception();
        @NotNull final Project project = projectRepository.findOneById(userId, projectId);
        if (project!=null && project.getUserId().equals(userId)) {
            @NotNull final Collection<Task> taskCollection = taskRepository.findAll();
            if(taskCollection==null) throw new EmptyCollectionException();
            taskCollection.removeIf(task -> task.getProjectId().equals(projectId));
        }
    }

    @Override
    public Collection<Task> findAllByUserId(@NotNull final String userId) throws Exception {
        if(userId.isEmpty()) throw new Exception();
        return taskRepository.findAllByUserId(userId);
    }

    @Override
    public Collection<Task> findAllByProjectId(@NotNull final String projectId) throws Exception {
        if(projectId.isEmpty()) throw new Exception();
        return taskRepository.findAllByProjectId(projectId);
    }

    @Override
    public boolean isEmptyTaskList() {
        return taskRepository.findAll().isEmpty();
    }

    @Override
    public void deleteTask( @NotNull final String userId, @NotNull final String taskId) throws Exception {
        if(taskId.isEmpty() || userId.isEmpty()) throw new Exception();
        @NotNull final Task task = taskRepository.findOne(taskId);
        if (task == null || task.getProjectId()==null || task.getProjectId().isEmpty()) return;
        @NotNull final Project project = projectRepository.findOneById(userId, task.getProjectId());
        @NotNull final String projectUserId = project.getUserId();
        if (!projectUserId.isEmpty() && project.getUserId().equals(userId)) taskRepository.remove(task);
    }


    @Override
    public void deleteAllTaskByUserId(@NotNull final String userId) throws Exception {
        if(userId.isEmpty()) throw new Exception();
        @NotNull final Collection<Task> taskCollection = taskRepository.findAllByUserId(userId);
        if(taskCollection==null || taskCollection.isEmpty()) throw new EmptyCollectionException();
        taskRepository.removeAllTasksCollection(taskCollection);
    }

    @Override
    public void deleteAllTaskByProjectId(@NotNull final String userId, @NotNull final String projectId) throws Exception {
        if(userId.isEmpty() || projectId.isEmpty()) throw new Exception();
        @NotNull final Project project = projectRepository.findOneById(userId, projectId);
        if(project!=null) throw new Exception();
        @NotNull final Collection<Task> taskCollection = taskRepository.findAllByProjectId(projectId);
        if(taskCollection==null || taskCollection.isEmpty()) throw new EmptyCollectionException();
        taskRepository.removeAllTasksCollection(taskCollection);
    }

    @Override
    public Task findTaskByName(@NotNull final String userId, @NotNull final String taskName) throws Exception {
        if(userId.isEmpty() || taskName.isEmpty())throw new Exception();
        @NotNull final Task task = taskRepository.findOneByName(userId, taskName);
        if(task == null) return null;
        return task;
    }

    @Override
    public List<Task> sortTasksByEndDate(@NotNull final String userId) throws Exception{
        if(userId.isEmpty()) throw new Exception();
        return taskRepository.sortByEndDate(userId);
    }

    @Override
    public List<Task> sortTasksByStartDate(@NotNull final String userId) throws Exception{
        if(userId.isEmpty()) throw new Exception();
        return taskRepository.sortByStartDate(userId);
    }

    @Override
    public List<Task> sortTasksBySystemDate(@NotNull final String userId) throws Exception{
        if(userId.isEmpty()) throw new Exception();
        return taskRepository.sortBySystemDate(userId);
    }

    @Override
    public List<Task> sortTasksByStatus(String userId) throws Exception {
        if(userId.isEmpty()) throw new Exception();
        return taskRepository.sortByStatus(userId);
    }

    @Override
    public void addAll(List<Task> projects) {
        projects.addAll(taskRepository.findAll());
    }

}
