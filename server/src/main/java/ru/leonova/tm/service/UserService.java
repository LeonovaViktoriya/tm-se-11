package ru.leonova.tm.service;

import org.apache.commons.codec.digest.DigestUtils;
import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.api.repository.IUserRepository;
import ru.leonova.tm.api.service.IUserService;
import ru.leonova.tm.entity.User;
import ru.leonova.tm.enumerated.RoleType;
import ru.leonova.tm.utils.PasswordHashUtil;

import java.util.Collection;
import java.util.List;

public final class UserService extends AbstractService<User> implements IUserService {

    final private IUserRepository userRepository;
    private User currentUser;

    public UserService(IUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User getCurrentUser() {
        return currentUser;
    }

    @Override
    public void create(@NotNull final User user) {
        user.setRoleType(RoleType.USER.getRole());
        userRepository.persist(user);
    }

    @Override
    public void load(@NotNull List<User> list) throws Exception {
        if (list.isEmpty()) throw new Exception();
        userRepository.load(list);
    }

    @Override
    public void logOut(@NotNull final User user){currentUser=null;}

    @Override
    public Collection<User> getCollection() {
        return userRepository.findAll();
    }

    @Override
    public User authorizationUser(@NotNull final String login, @NotNull final String password) throws Exception {
        if (login.isEmpty() || password.isEmpty()) throw new Exception();
        @NotNull final Collection<User> userCollection = userRepository.findAll();
        for (@NotNull final User user : userCollection) {
            if (user.getLogin().equals(login) && user.getPassword().equals(PasswordHashUtil.md5(password))) {
                currentUser = user;
                return currentUser;
            }
        }
        return null;
    }

    @Override
    public boolean isAuth() {
        return currentUser != null;
    }

    @Override
    public User getById(@NotNull final String userId) throws Exception {
        if (userId.isEmpty()) throw new Exception();
        return userRepository.findOne(userId);
    }

    @Override
    public String md5Apache(@NotNull final String password) throws Exception {
        if(password.isEmpty()) throw new Exception();
        return DigestUtils.md5Hex(password);
    }

    @Override
    public void addAll(List<User> users) {
        users.addAll(userRepository.findAll());
    }

    @Override
    public void adminRegistration(@NotNull final String login, @NotNull String password) throws Exception {
        if(login.isEmpty() || password.isEmpty()) throw new Exception();
        @NotNull final User admin = new User("admin", md5Apache("admin"));
        admin.setRoleType(RoleType.ADMIN.getRole());
        userRepository.persist(admin);
    }

}
