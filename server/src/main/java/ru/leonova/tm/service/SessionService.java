package ru.leonova.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.api.ServiceLocator;
import ru.leonova.tm.api.service.ISessionService;
import ru.leonova.tm.entity.Session;
import ru.leonova.tm.entity.User;
import ru.leonova.tm.enumerated.RoleType;
import ru.leonova.tm.exeption.AccessException;
import ru.leonova.tm.exeption.EmptyArgumentException;
import ru.leonova.tm.repository.SessionRepository;
import ru.leonova.tm.utils.SignatureUtil;

import java.util.Collection;
import java.util.UUID;

public final class SessionService extends AbstractService<Session> implements ISessionService {

    private final ServiceLocator serviceLocator;
    private final SessionRepository sessionRepository;

    public SessionService(ServiceLocator serviceLocator, SessionRepository sessionRepository) {
        this.serviceLocator = serviceLocator;
        this.sessionRepository = sessionRepository;
    }

    @Override
    public boolean valid(@NotNull final Session session) throws Exception {
        if (session.getSessionId().isEmpty() || session.getUserId().isEmpty() || session.getTimestamp().intValue() == 0 || session.getSignature().isEmpty() || session.getRoleType().getRole().isEmpty())
            throw new Exception("fields is empty");
        @NotNull final Session clone = session.clone();
        @NotNull final String sessionSignature = session.getSignature();
        clone.setSignature(null);
        final String cloneSignature = SignatureUtil.sign(clone);
        if (!sessionSignature.equals(cloneSignature)) throw new Exception("signatures not equals");
        @NotNull final long TimeStampCurrent = System.currentTimeMillis();
        if (TimeStampCurrent > session.getTimestamp() + (30 * 60 * 1000)) throw new Exception("Session time out");
        return true;
    }

    @Override
    public void closeSession(@NotNull final Session session) throws Exception {
        sessionRepository.remove(session);
    }

    @Override
    public Session openSession(@NotNull final String login, @NotNull final String password) throws Exception {
        if (login.isEmpty() || password.isEmpty()) throw new Exception();
        @NotNull final User user = serviceLocator.getUserService().authorizationUser(login, password);
        if (user == null) throw new Exception("This user not found");
        @NotNull final Session session = new Session();
        session.setSessionId(UUID.randomUUID().toString());
        session.setUserId(user.getUserId());
        session.setRoleType(RoleType.USER);
        session.setTimestamp(System.currentTimeMillis());
        final String signature = SignatureUtil.sign(session);
        if (signature == null) throw new Exception("Signature is null!");
        session.setSignature(signature);
        return session;
    }

    @Override
    Collection<Session> getCollection() {
        return sessionRepository.findAll();
    }

    @Override
    public Session getSessionByUserId(String userId) {
        return sessionRepository.findSessionByUserId(userId);
    }
}
